from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('addTodo/', views.addTodo),
    path('completed/<int:_id>/', views.completed),
    path('delete/<int:_id>/', views.delete),
    path('description/<int:_id>/', views.description),
]
