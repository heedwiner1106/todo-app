from django.shortcuts import redirect, render
from .models import Todo_list as todo_model, Priority as pri_model, Student as  stu_list
# Create your views here.
def index(request):
    todo_list = todo_model.objects.filter().order_by('todo_completed', 'todo_priority')
    student_list =  stu_list.objects.filter()
    YEAR_IN_SCHOOL_CHOICES = {
        'FR': 'Freshman',
        'SO': 'Sophomore',
        'JR': 'Junior',
        'SR': 'Senior',
        'GR': 'Graduate',
    }
    name_stu = []
    for stu in student_list:
        name_stu.append(''.join(YEAR_IN_SCHOOL_CHOICES[stu.year_in_school]))
    return render(request, 'pages/index.html', {'todo_list': todo_list, 'student': student_list, 'year_in_shool': name_stu})

def addTodo(request):
    if request.method == 'POST':
        todo_name = request.POST['todo_name']
        priority = request.POST['priority']
        todo_priority = pri_model.objects.get(id = priority)
        todo = todo_model.objects.create(todo_name = todo_name, todo_priority = todo_priority)
        todo.save()
        return redirect('/home/')

    else:
        return redirect('/home/')


def description(request,_id):
    if request.method == 'POST':
        id = _id
        description = request.POST['description']
        todo = todo_model.objects.filter(todo_id = id)
        todo.update(todo_description = description)
        return redirect('/home/')
    else:
        return redirect('/admin/')

def completed(request,_id):
    if request.method == 'POST':
        id = _id
        todo = todo_model.objects.filter(todo_id = id)
        todo.update(todo_completed = not todo[0].todo_completed)
        return redirect('/home/')
    else:
        return redirect('/admin/')

def delete(request,_id):
    if request.method == 'POST':
        id = _id
        todo = todo_model.objects.filter(todo_id = id)
        todo.delete()
        return redirect('/home/')
    else:
        return redirect('/admin/')