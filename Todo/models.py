from decimal import DefaultContext
from django.db import models as m
# Create your models here.


class Priority(m.Model):
    id = m.AutoField(primary_key=True)
    name = m.CharField(max_length=50, null=False)

    def __str__(self):
        return f"{self.id}, {self.name}"

class Todo_list(m.Model):
    todo_id = m.AutoField(primary_key=True)
    todo_name = m.CharField(max_length=50, null=False)
    todo_description = m.TextField(null=True, default=None)
    todo_priority = m.ForeignKey(Priority, on_delete=m.CASCADE, default=2)
    todo_completed = m.BooleanField(default=False)
    
    def __str__(self):
        return f"{self.todo_id},{self.todo_name},{self.todo_description},{self.todo_priority},{self.todo_completed}"


class Student(m.Model):
    FRESHMAN = 'FR'
    SOPHOMORE = 'SO'
    JUNIOR = 'JR'
    SENIOR = 'SR'
    GRADUATE = 'GR'
    YEAR_IN_SCHOOL_CHOICES = [
        (FRESHMAN, 'Freshman'),
        (SOPHOMORE, 'Sophomore'),
        (JUNIOR, 'Junior'),
        (SENIOR, 'Senior'),
        (GRADUATE, 'Graduate'),
    ]
    year_in_school = m.CharField(
        max_length=2,
        choices=YEAR_IN_SCHOOL_CHOICES,
        default=FRESHMAN,
    )

    def is_upperclass(self):
        return self.year_in_school in {self.JUNIOR, self.SENIOR}