from re import L
from django.contrib import admin
from .models import Priority, Todo_list, Student
# Register your models here.

class TodoAdmin(admin.ModelAdmin):
    list_display = ('todo_id', 'todo_name', 'todo_description', 'todo_priority', 'todo_completed')
    search_fields = ['todo_id', 'todo_name']
    list_filter = ('todo_name', 'todo_priority', 'todo_completed')
admin.site.register(Todo_list, TodoAdmin)

class PriorityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['id', 'name']
admin.site.register(Priority, PriorityAdmin)

class StudentAdmin(admin.ModelAdmin):
    list_display = ['year_in_school']
admin.site.register(Student, StudentAdmin)